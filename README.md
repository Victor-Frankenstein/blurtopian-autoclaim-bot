# Blurtopian Auto-Claim Bot

To run the bot locally, you need to have `npm` installed. There are several tutorials available on the net. The installation I used was the one which came with nodejs. Installation steps are [here](https://github.com/nodesource/distributions/blob/master/README.md).

## 1.1. Installation

Open up a terminal or shell (depending on your OS) and run the following commands:

1. `git clone https://gitlab.com/blurt/openblurt/blurtopian/blurtopian-autoclaim-bot.git`
2. `cd blurtopian-autoclaim-bot`
3. `npm install`

## 1.2. Bot Configuration

1. Create an `accounts.json` file in the project's root directory (or rename `accounts.json.example` to `accounts.json`)
2. Create an account and posting key pairs (these will be the accounts that will be auto-claimed)
```
{
  "guest1": "5JRaypasxMx1L97ZUX7YuC5Psb5EAbF821kkAGtBj7xCJFQcbLg",
  "guest2": "5JRaypasxMx1L97ZUX7YuC5Psb5EAbF821kkAGtBj7xCJFQcbLg"
}
```
3. (Optional) Create an `.env` file and add an environment variable `CLAIM_INTERVAL` to set interval (in `minutes`) to check for pending claim account balances  (default interval is `360 minutes (6 hours)`)

   1. `touch .env` (if using bash shell)
   2. `nano .env` or `vi .env`
   3. `cat .env` to check `.env`'s conent
   ```
   CLAIM_INTERVAL=360
   ```

## 1.3. Run

Running the following command should run the bot. 

1. Run `npm start`

![image.png](https://images.hive.blog/DQmURJazLGSSDKZpPxR5FYz11LWQXuwbkHeJ1BMRqGPuC5W/image.png)


## 1.4. Run Forever

If you have a server (I use [Privex](https://pay.privex.io/order/?r=eastmaels)), you can opt to run the app perpetually via pm2.

### 1.4.1. Install pm2

1. `npm install pm2 -g`
2. `pm2 startup`
3. Copy the command generated by the previous step
    * For more info, refer to https://pm2.keymetrics.io/docs/usage/startup/
4. `pm2 start npm -- start`
5. `pm2 save`
6. `pm2 l`

Step 6 should output the running npm process. When you logout and re-login to your VPS, and issue the command `pm2 l`, the previously running process should display and display status as `running`.